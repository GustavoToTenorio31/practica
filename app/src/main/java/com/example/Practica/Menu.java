package com.example.Practica;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class Menu extends AppCompatActivity {
    String url="https://notificacionupt.andocodeando.net/api/todosUsuarios";
    String urll="https://notificacionupt.andocodeando.net/api/detallesUsuario";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        WebView most = (WebView) findViewById(R.id.Obuss);
        most.setWebViewClient(new MyWebViewClient());
        WebSettings config = most.getSettings();
        config.setJavaScriptEnabled(true);
        most.loadUrl(url);

        WebView detll = (WebView) findViewById(R.id.Detuss);
        detll.setWebViewClient(new MyClientNew());
        WebSettings det = detll.getSettings();
        det.setJavaScriptEnabled(true);
        detll.loadUrl(urll);

        Button regm = findViewById(R.id.btnclrs);
        regm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent r = new Intent(Menu.this, MainActivity.class);
                startActivity(r);
                finish();
            }
        });

    }
    private class MyWebViewClient extends WebViewClient{
        public  boolean shouldOverrideUrlLoading(WebView view,String url){
            view.loadUrl(url);
            return true;
        }
    }
    private class MyClientNew extends WebViewClient{
        public boolean shouldOverrideUrlLoading(WebView view,String urll){
            view.loadUrl(urll);
            return true;
        }

    }
}
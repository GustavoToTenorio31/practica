package com.example.Practica.Api.Servicios;

import com.example.Practica.ViewModels.PeticionNoticia;
import com.example.Practica.ViewModels.RegistroUsuario;
import com.example.Practica.ViewModels.PeticionLogin;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.GET;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("apiLogin/crearUsuario")
    Call<RegistroUsuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("apiLogin/login")
    Call<PeticionLogin>getLogin(@Field("username")String correo,@Field("password")String contrasenia);

    @GET("apiLogin/todasNot")
    Call<PeticionNoticia>getNoticias();

}
